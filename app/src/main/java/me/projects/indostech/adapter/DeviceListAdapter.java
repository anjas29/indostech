package me.projects.indostech.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import me.projects.indostech.R;
import me.projects.indostech.objects.Device;

public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListViewHolder> {
    private Context context;
    private ArrayList<Device> itemList;

    public DeviceListAdapter(Context context, ArrayList<Device> itemList) {
        super();
        this.context = context;
        this.itemList = itemList;
    }

    @Override
    public DeviceListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_device, null);
        DeviceListViewHolder view = new DeviceListViewHolder(layoutView);
        return view;
    }

    @Override
    public void onBindViewHolder(DeviceListViewHolder holder, int position) {
        holder.deviceLabel.setText(itemList.get(position).getName());

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }
}
