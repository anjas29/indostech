package me.projects.indostech.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import me.projects.indostech.DetailDeviceActivity;
import me.projects.indostech.MainFragment;
import me.projects.indostech.R;

public class DeviceListViewHolder extends RecyclerView.ViewHolder {
    TextView deviceLabel;

    public DeviceListViewHolder(final View itemView) {
        super(itemView);
        deviceLabel = (TextView) itemView.findViewById(R.id.device_label);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(itemView.getContext(), DetailDeviceActivity.class);
                intent.putExtra("name", MainFragment.deviceItemList.get(getAdapterPosition()).getName());
                intent.putExtra("address", MainFragment.deviceItemList.get(getAdapterPosition()).getAddress());
                intent.putExtra("connected", MainFragment.deviceItemList.get(getAdapterPosition()).isConnected());
                itemView.getContext().startActivity(intent);
            }
        });

    }
}
