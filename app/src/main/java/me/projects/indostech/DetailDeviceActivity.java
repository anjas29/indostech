package me.projects.indostech;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class DetailDeviceActivity extends AppCompatActivity implements LocationListener {
    RelativeLayout alarmButton, locationButton, unlockModeButton, alarmSetButton, notificationSetButton, securitySetButton, deviceImage;
    ImageView deviceImageButton;
    TextView deviceNameView, deviceNameButton;
    String deviceName, latitude, longitude;
    LocationManager locationManager;
    boolean locationCheck = false;
    boolean alarm = true;
    boolean alarmRing = false;
    boolean notification = true;
    boolean security = false;

    Handler bluetoothIn;

    final int handlerState = 0;                        //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    private ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    // String for MAC address
    private static String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_device);

        deviceNameView = findViewById(R.id.device_view);

        alarmButton = findViewById(R.id.alarm_button);
        locationButton = findViewById(R.id.location_button);
        unlockModeButton = findViewById(R.id.unlock_button);
        alarmSetButton = findViewById(R.id.alarm_set_button);
        notificationSetButton = findViewById(R.id.notification_set_button);
        securitySetButton = findViewById(R.id.security_set_button);

        deviceImage = findViewById(R.id.device_image);

        deviceImageButton = findViewById(R.id.image_edit);
        deviceNameButton = findViewById(R.id.device_edit);

        alarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alarmAction();
            }
        });
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationAction();
            }
        });
        alarmSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alarmSetAction();
            }
        });
        notificationSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notificationSetAction();
            }
        });
        securitySetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                securitySetAction();
            }
        });


        Bundle extras = getIntent().getExtras();
        deviceName = extras.getString("name");
        deviceNameView.setText(deviceName);
        address = extras.getString("address");
        connectDevice();


    }

    public void onPause()
    {
        super.onPause();
        try
        {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    public void connectDevice(){
        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {

            }
        };

        btAdapter = BluetoothAdapter.getDefaultAdapter();

        //create device and set the MAC address
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
            Toast.makeText(this, "Bluetooth Connected", Toast.LENGTH_SHORT).show();
            Log.d("DEBUG", "Try to Connect");
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed!", Toast.LENGTH_LONG).show();
            Log.d("DEBUG", "Soecket Creation Failure");
        }
        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
            Log.d("DEBUG", "BT Connect");
        } catch (IOException e) {
            try
            {
                btSocket.close();
            } catch (IOException e2)
            {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();
        mConnectedThread.write("b");
        Toast.makeText(this, "Bluetooth Connected", Toast.LENGTH_SHORT).show();
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    public void alarmAction() {
        if(alarmRing){
            mConnectedThread.write("c");
            Toast.makeText(this,"Set Alarm Off",Toast.LENGTH_SHORT).show();
            alarmRing = false;
        }else{
            mConnectedThread.write("a");
            Toast.makeText(this,"Alarm Ringing",Toast.LENGTH_SHORT).show();
            alarmRing = true;
        }
    }

    public void locationAction() {
        try{
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 5, this);
            Log.d("ERROR", "Started");
        }catch(SecurityException e){
            e.printStackTrace();
            Log.d("ERROR", "GPS ERROR");
        }

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:<" + latitude  + ">,<" + longitude+ ">?q=<" + latitude  + ">,<" + longitude + ">(" + deviceName + ")"));
            startActivity(intent);

    }

    public void unlockAction(){}
    public void alarmSetAction(){
        if (alarm) {
            mConnectedThread.write("c");
            Toast.makeText(this,"Set Alarm Off",Toast.LENGTH_SHORT).show();
            alarm = false;
        }else{
            mConnectedThread.write("b");
            Toast.makeText(this,"Set Alarm On",Toast.LENGTH_SHORT).show();
            alarm = true;
        }
    }
    public void notificationSetAction(){
        //mConnectedThread.write("c");
        if(notification){
            Toast.makeText(this,"Set Notification Off",Toast.LENGTH_SHORT).show();
            notification = false;
        }else{
            Toast.makeText(this,"Set Notification On",Toast.LENGTH_SHORT).show();
            notification = true;
        }
    }
    public void securitySetAction(){
        if(security){
            mConnectedThread.write("c");
            Toast.makeText(this,"Set Security Off",Toast.LENGTH_SHORT).show();
            security = false;
        }else{
            mConnectedThread.write("b");
            Toast.makeText(this,"Set Security On",Toast.LENGTH_SHORT).show();
            security = true;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = ""+location.getLatitude();
        longitude = ""+location.getLongitude();
        locationCheck = true;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        //creation of the connect thread
        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            try {
                //Create I/O streams for connection
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[256];
            int bytes;

            // Keep looping to listen for received messages
            while (true) {
                try {
                    bytes = mmInStream.read(buffer);            //read bytes from input buffer
                    String readMessage = new String(buffer, 0, bytes);
                    // Send the obtained bytes to the UI Activity via handler
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }
        //write method
        public void write(String input) {
            byte[] msgBuffer = input.getBytes();           //converts entered String into bytes
            try {
                mmOutStream.write(msgBuffer);                //write bytes over BT connection via outstream
                Log.d("DEBUG", "WRITE MESSAGE :" + input);
            } catch (IOException e) {
                //if you cannot write, close the application
                Toast.makeText(getBaseContext(), "Connection Failure", Toast.LENGTH_LONG).show();
                finish();

            }
        }
    }
}
