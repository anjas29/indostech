package me.projects.indostech;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class UpdateProfilActivity extends AppCompatActivity {
    TextView nameView, phoneView;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profil);

        nameView = findViewById(R.id.nameView);
        phoneView = findViewById(R.id.phoneView);
        saveButton = findViewById(R.id.saveButton);

        Bundle extra = getIntent().getExtras();
        String name = extra.getString("name");
        String phone = extra.getString("phone");

        nameView.setText(name);
        phoneView.setText(phone);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("users");
        final String email = user.getEmail();

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    for(DataSnapshot object : dataSnapshot.getChildren()){
                        String nEmail = object.child("email").getValue().toString();
                        if(nEmail.equals(email)){
                            object.child("name").getRef().setValue(nameView.getText().toString());
                            object.child("phone").getRef().setValue(phoneView.getText().toString());
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            endActivity();
            }
        });
    }

    public void endActivity(){
        Toast.makeText(this,"Profile Updated", Toast.LENGTH_SHORT).show();
        finish();
    }
}
