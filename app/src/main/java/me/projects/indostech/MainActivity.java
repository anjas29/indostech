package me.projects.indostech;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private MainFragment mDeviceListFragment;
    private BluetoothAdapter BTAdapter;

    public static int REQUEST_BLUETOOTH = 1;

    MainFragment mainFragment;

    private int[] tabIcons = {
            R.drawable.ic_guide,
            R.drawable.ic_wisata,
            R.drawable.ic_hotel,
            R.drawable.ic_kuliner
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        //setupBluetooth();
        setupTabIcons();

        tabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager){
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.colorMain);
                ((ImageView)tab.getCustomView().findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                ((TextView)tab.getCustomView().findViewById(R.id.tabText)).setTextColor(tabIconColor);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);
                int tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.gray);
                ((ImageView)tab.getCustomView().findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
                ((TextView)tab.getCustomView().findViewById(R.id.tabText)).setTextColor(tabIconColor);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
            }
        });
    }

    public void setupBluetooth(){
        BTAdapter = BluetoothAdapter.getDefaultAdapter();
        if (BTAdapter == null) {
            new AlertDialog.Builder(this)
                    .setTitle("Not compatible")
                    .setMessage("Your phone does not support Bluetooth")
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        if (!BTAdapter.isEnabled()) {
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBT, REQUEST_BLUETOOTH);
        }

        mainFragment = MainFragment.newInstance(BTAdapter);
    }


    public void setupTabIcons(){
        View tab1 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab1.findViewById(R.id.tabText)).setText("Device");
        ((ImageView)tab1.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_bluetooth_black_48dp);
        int tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.colorMain);
        ((ImageView)tab1.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab1.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(0).setCustomView(tab1);

        View tab2 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab2.findViewById(R.id.tabText)).setText("Help");
        ((ImageView)tab2.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_help_black_48dp);
        tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.black);
        ((ImageView)tab2.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab2.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(1).setCustomView(tab2);

        View tab3 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab3.findViewById(R.id.tabText)).setText("Setting");
        ((ImageView)tab3.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_settings_black_48dp);
        tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.black);
        ((ImageView)tab3.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab3.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(2).setCustomView(tab3);

        View tab4 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tabs, null);
        ((TextView)tab4.findViewById(R.id.tabText)).setText("Account");
        ((ImageView)tab4.findViewById(R.id.tabIcon)).setImageResource(R.drawable.ic_account_circle_black_48dp);
        tabIconColor = ContextCompat.getColor(getBaseContext(), R.color.black);
        ((ImageView)tab4.findViewById(R.id.tabIcon)).setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
        ((TextView)tab4.findViewById(R.id.tabText)).setTextColor(tabIconColor);
        tabLayout.getTabAt(3).setCustomView(tab4);

    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MainFragment(), "Device");
        adapter.addFragment(new HelpFragment(), "Help");
        adapter.addFragment(new SettingFragment(), "Setting");
        adapter.addFragment(new AccountFragment(), "Account");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.logout_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_logout:
                FirebaseAuth.getInstance().signOut();
                //user.updateProfile(profileUpdates);
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
