package me.projects.indostech;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    TextView nameView, emailView, phoneView, editView;
    String name, phone;
    ProgressBar loadingView;
    LinearLayout accountView;


    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_account, container, false);
        nameView = v.findViewById(R.id.name_view);
        emailView = v.findViewById(R.id.email_view);
        phoneView = v.findViewById(R.id.phone_view);
        editView = v.findViewById(R.id.edit_view);

        accountView = v.findViewById(R.id.accountView);
        loadingView = v.findViewById(R.id.loadingView);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("users");
        final String email = user.getEmail();
        emailView.setText(email);

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot object : dataSnapshot.getChildren()){
                    String nEmail = object.child("email").getValue().toString();
                    if(nEmail.equals(email)){
                        loadingView.setVisibility(View.GONE);
                        accountView.setVisibility(View.VISIBLE);
                        
                        name = object.child("name").getValue().toString();
                        phone = object.child("phone").getValue().toString();

                        nameView.setText(name);
                        phoneView.setText(phone);
                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        editView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(),UpdateProfilActivity.class);
                intent.putExtra("name",name);
                intent.putExtra("phone",phone);
                startActivity(intent);
            }
        });

        return v;
    }

}
