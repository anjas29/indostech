package me.projects.indostech;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Set;

import me.projects.indostech.adapter.DeviceListAdapter;
import me.projects.indostech.adapter.NewDeviceListAdapter;
import me.projects.indostech.objects.Device;
import me.projects.indostech.objects.DeviceItem;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {
    RecyclerView deviceListView, newListView;
    LinearLayoutManager layoutManager, layoutManager2;
    public static ArrayList<Device> deviceItemList, newItemList;

    private ArrayList <DeviceItem> itemList;

    private static BluetoothAdapter bTAdapter;
    private ArrayAdapter<DeviceItem> mAdapter;

    public static int REQUEST_BLUETOOTH = 1;

    private final BroadcastReceiver bReciever = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.d("DEVICELIST", "Bluetooth device found\n");
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Create a new device item
                DeviceItem newDevice = new DeviceItem(device.getName(), device.getAddress(), false);
                // Add it to our adapter
                mAdapter.add(newDevice);
                mAdapter.notifyDataSetChanged();
            }
        }
    };

    public static MainFragment newInstance(BluetoothAdapter adapter) {
        MainFragment fragment = new MainFragment();
        bTAdapter = adapter;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bTAdapter = BluetoothAdapter.getDefaultAdapter();

        Log.d("DEVICELIST", "Super called for DeviceListFragment onCreate\n");
        itemList = new ArrayList<DeviceItem>();
        deviceItemList = new ArrayList<Device>();
        newItemList = new ArrayList<Device>();

        Set<BluetoothDevice> pairedDevices = bTAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                DeviceItem newDevice= new DeviceItem(device.getName(),device.getAddress(),false);
                itemList.add(newDevice);
                deviceItemList.add(new Device(device.getName(),device.getAddress(), false));
            }
        }

        // If there are no devices, add an item that states so. It will be handled in the view.
        if(itemList.size() == 0) {
            itemList.add(new DeviceItem("No Devices", "", false));
            deviceItemList.add(new Device("No Device","", false));
        }

        newItemList.add(new Device("No Device","", false));
        Log.d("DEVICELIST", "DeviceList populated\n");

        mAdapter = new NewDeviceListAdapter(getActivity(), itemList, bTAdapter);

        Log.d("DEVICELIST", "Adapter created\n");
    }

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        deviceListView = view.findViewById(R.id.device_list);
        newListView = view.findViewById(R.id.new_list);

        getDeviceList();

        layoutManager= new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL, false);
        layoutManager2= new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL, false);

        deviceListView.setHasFixedSize(true);
        deviceListView.setLayoutManager(layoutManager);
        newListView.setHasFixedSize(true);
        newListView.setLayoutManager(layoutManager2);

        DeviceListAdapter adapterDevice = new DeviceListAdapter(view.getContext(), deviceItemList);
        DeviceListAdapter adapterNewDevice = new DeviceListAdapter(view.getContext(), newItemList);


        deviceListView.setAdapter(adapterDevice);
        newListView.setAdapter(adapterNewDevice);

        /*IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        getActivity().registerReceiver(bReciever, filter);
        bTAdapter.startDiscovery();*/

        return view;
    }

    public void getDeviceList(){
    }

}
